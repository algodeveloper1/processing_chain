import sys, os
## Add the algorithms directory to the python path MUST BE AUTOMATIC IN THE FUTURE
sys.path.append('/projects/slctosig0/src/')
sys.path.append('/projects/speckle_filtering/src/')
sys.path.append('/projects/slrtogrd/src/')

#import the algorithm to use in the processing
import SLCtoSig0
import speckle_filter
import SLRtoGRD

# open the config file

from properties.p import Property
import quicklook_raster
if os.path.isfile('/projects/processing_chain/configuration.properties'):
    configfile='/projects/processing_chain/configuration.properties'

    prop=Property()
    prop_dict=prop.load_property_files(configfile)

    SLCtoSig0.prog1(prop_dict['inputfile'], prop_dict['xmlfile'],prop_dict['output1'])
    quicklook_raster.main(prop_dict['output1'])
    speckle_filter.prog2(prop_dict['output1'], prop_dict['size'],prop_dict['output2'])
    quicklook_raster.main(prop_dict['output2'])
    #SLRtoGRD.SlrToGrdProj(prop_dict['output2'], prop_dict['azfile'],prop_dict['rgfile'],prop_dict['output3'])



